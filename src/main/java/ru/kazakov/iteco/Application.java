package ru.kazakov.iteco;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.context.Bootstrap;

public final class Application {

    public static void main(@NotNull String[] args) throws Exception {
        new Bootstrap().init();
    }

}
