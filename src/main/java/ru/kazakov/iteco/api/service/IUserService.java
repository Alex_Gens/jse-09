package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    public String getName(@Nullable final String id) throws Exception;

    @Nullable
    public User findByLogin(@Nullable final String login) throws Exception;

    public boolean contains(@Nullable final String login) throws Exception;

}
