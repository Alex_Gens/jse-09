package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    public String getName(@NotNull final String id);

    @Nullable
    public User findByLogin(@NotNull final String login) throws Exception;

    public boolean contains(@NotNull final String login) throws Exception;

}
