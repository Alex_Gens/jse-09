package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.api.service.ITerminalService;
import ru.kazakov.iteco.api.service.IUserService;

public interface ServiceLocator {

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public ITerminalService getTerminalService();

}
