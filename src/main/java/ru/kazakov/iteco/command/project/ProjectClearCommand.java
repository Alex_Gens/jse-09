package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ProjectClearCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-clear";

    @Getter
    @NotNull
    private final String description = "Remove all projects.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        projectService.removeAll(currentUserId);
        serviceLocator.getTaskService().removeAllWithProjects(currentUserId);
        terminalService.write("[ALL PROJECTS REMOVED]");
        terminalService.write("Projects successfully removed!");
        terminalService.separateLines();
    }

}
