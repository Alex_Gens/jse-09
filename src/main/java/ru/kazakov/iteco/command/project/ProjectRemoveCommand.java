package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Project;

@NoArgsConstructor
public final class ProjectRemoveCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-remove";

    @Getter
    @NotNull
    private final String description = "Remove project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final Project project = getProjectByPart(currentUserId);
        if (project == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.removeWithProject(currentUserId, project.getId());
        projectService.remove(project.getId());
        terminalService.write("[REMOVED]");
        terminalService.write("Project successfully removed!");
        terminalService.separateLines();
    }

}
