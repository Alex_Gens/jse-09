package ru.kazakov.iteco.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@NoArgsConstructor
public abstract class ProjectAbstractCommand extends AbstractCommand {

    @Nullable
    protected IProjectService projectService;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @Nullable
    protected Project getProjectByPart(@NotNull final String currentUserId) throws Exception {
        if (terminalService == null) throw new Exception();
        if (projectService == null) throw new Exception();
        terminalService.write("ENTER PART OF PROJECT NAME OR DESCRIPTION: ");
        @NotNull final String part = terminalService.enterIgnoreEmpty();
        terminalService.separateLines();
        @Nullable final List<Project> projectsByName = projectService.findAllByName(part, currentUserId);
        @Nullable final List<Project> projectsByInfo = projectService.findAllByInfo(part, currentUserId);
        if (projectsByName == null) throw new Exception();
        if (projectsByInfo == null) throw new Exception();
        if (projectsByName.isEmpty() && projectsByInfo.isEmpty()) {
            terminalService.write("Project doesn't exist. Use \"project-list\" to show all projects.");
            terminalService.separateLines();
            return null;
        }
        int counter = 1;
        @NotNull final Map<String, Project> projectsByNameMap = new TreeMap<>();
        @NotNull final Map<String, Project> projectsByInfoMap = new TreeMap<>();
        for (Project project : projectsByName) {
            projectsByNameMap.put(String.valueOf(counter), project);
            counter++;
        }
        for (Project project : projectsByInfo) {
            projectsByInfoMap.put(String.valueOf(counter), project);
            counter++;
        }
        @NotNull final Project project;
        while (true) {
            if (!projectsByNameMap.isEmpty()) terminalService.write("Found by name: ");
            projectsByNameMap.forEach((k, v) -> terminalService.write(k + ". " + v.getName()));
            if (!projectsByInfoMap.isEmpty()) terminalService.write("Found by description:");
            projectsByInfoMap.forEach((k, v) -> terminalService.write(k + ". " + v.getName()));
            terminalService.separateLines();
            terminalService.write("Select project.");
            terminalService.write("ENTER PROJECT NUMBER: ");
            @NotNull final String number = terminalService.enterIgnoreEmpty();
            if (!projectsByNameMap.containsKey(number) && !projectsByInfoMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            if (projectsByNameMap.containsKey(number)) {
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                project = projectsByNameMap.get(number);
                break;
            }
            if (projectsByInfoMap.containsKey(number)) {
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                project = projectsByInfoMap.get(number);
                break;
            }
        }
        return project;
    }

    @Nullable
    protected Task getTaskByPart(@NotNull final String currentUserId) throws Exception {
        if (terminalService == null) throw new Exception();
        if (serviceLocator == null) throw new Exception();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminalService.write("ENTER PART OF TASK NAME OR DESCRIPTION: ");
        @NotNull final String part = terminalService.enterIgnoreEmpty();
        terminalService.separateLines();
        @Nullable final List<Task> tasksByName = taskService.findAllByName(part, currentUserId);
        @Nullable final List<Task> tasksByInfo = taskService.findAllByInfo(part, currentUserId);
        if (tasksByName == null) throw new Exception();
        if (tasksByInfo == null) throw new Exception();
        if (tasksByName.isEmpty() && tasksByInfo.isEmpty()) {
            terminalService.write("Task doesn't exist. Use \"task-list\" to show all tasks.");
            terminalService.separateLines();
            return null;
        }
        int counter = 1;
        @NotNull final Map<String, Task> tasksByNameMap = new TreeMap<>();
        @NotNull final Map<String, Task> tasksByInfoMap = new TreeMap<>();
        for (Task task : tasksByName) {
            tasksByNameMap.put(String.valueOf(counter), task);
            counter++;
        }
        for (Task task : tasksByInfo) {
            tasksByInfoMap.put(String.valueOf(counter), task);
            counter++;
        }
        @NotNull final Task task;
        while (true) {
            if (!tasksByNameMap.isEmpty()) terminalService.write("Found by name: ");
            tasksByNameMap.forEach((k, v) -> terminalService.write(k + ". " + v.getName()));
            if (!tasksByInfoMap.isEmpty()) terminalService.write("Found by description:");
            tasksByInfoMap.forEach((k, v) -> terminalService.write(k + ". " + v.getName()));
            terminalService.separateLines();
            terminalService.write("Select task.");
            terminalService.write("ENTER TASK NUMBER: ");
            @NotNull final String number = terminalService.enterIgnoreEmpty();
            if (!tasksByNameMap.containsKey(number) && !tasksByInfoMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            if (tasksByNameMap.containsKey(number)) {
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                task = tasksByNameMap.get(number);
                break;
            }
            if (tasksByInfoMap.containsKey(number)) {
                terminalService.write("[CORRECT]");
                terminalService.separateLines();
                task = tasksByInfoMap.get(number);
                break;
            }
        }
        return task;
    }

}
