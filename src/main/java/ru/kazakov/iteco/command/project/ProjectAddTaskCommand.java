package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;

@NoArgsConstructor
public final class ProjectAddTaskCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-add-task";

    @Getter
    @NotNull
    private final String description = "Add task to project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final String currentUserId = currentState.getCurrentUser().getId();
        @Nullable final Project project = getProjectByPart(currentUserId);
        if (project == null) return;
        @Nullable final Task task = getTaskByPart(currentUserId);
        if (task == null) return;
        if (task.getProjectId() == null) {
            task.setProjectId(project.getId());
            terminalService.write("Task successfully added!");
            terminalService.separateLines();
            return;
        }
        if (task.getProjectId().equals(project.getId())) {
            terminalService.write("Task is already added. Use project-list-tasks to see tasks in project.");
            terminalService.separateLines();
            return;
        }
        task.setProjectId(project.getId());
        terminalService.write("Task successfully added!");
        terminalService.separateLines();
    }

}
