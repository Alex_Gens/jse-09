package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;

@NoArgsConstructor
public final class UserUpdateCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-update";

    @Getter
    @NotNull
    private final String description = "Update user name in profile";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (userService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final User currentUser = currentState.getCurrentUser();
        @Nullable User user = currentState.getCurrentUser();
        if (currentUser == null) throw new Exception();
        if (user == null) throw new Exception();
        if (currentUser.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to update user's profile.   [" + user.getRoleType().getDisplayName().toUpperCase() + "]");
            terminalService.write("ENTER LOGIN: ");
            final String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userService.findByLogin(login);
        }
        if (user == null) throw new Exception();
        terminalService.write("ENTER NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty();
        user.setName(name);
        terminalService.write("[UPDATED]");
        terminalService.write("Name successfully updated!");
        terminalService.separateLines();
    }

}
