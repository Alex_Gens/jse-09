package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
public final class UserGetCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-get";

    @Getter
    @NotNull
    private final String description = "Show user profile information.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (userService == null) throw new Exception();
        if (currentState.getCurrentUser() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable User user = currentState.getCurrentUser();
        if (user.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to get user's profile.   [" + user.getRoleType().getDisplayName().toUpperCase() + "]");
            terminalService.write("ENTER LOGIN: ");
            String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userService.contains(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userService.findByLogin(login);
        }
        if (user == null) throw new Exception();
        terminalService.write("Profile information: ");
        @Nullable final String name = user.getName();
        boolean nameIsExist = name != null && !name.isEmpty();
        if (nameIsExist) terminalService.write("Name: " + name);
        @Nullable final String login = user.getLogin();
        final boolean loginIsExist = login != null && !login.isEmpty();
        if (loginIsExist) terminalService.write("Login: " + login);
        if (!nameIsExist && !loginIsExist) {
            terminalService.write("Profile has no information.");
            terminalService.separateLines();
            return;
        }
        @Nullable final Date dateStart = user.getDateStart();
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        if (dateStart != null) {
            terminalService.write("Registration date: " + dateFormat.format(dateStart));
            terminalService.separateLines();
        }
    }

}
