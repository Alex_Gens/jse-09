package ru.kazakov.iteco.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public abstract class UserAbstractCommand extends AbstractCommand {

    @Nullable
    protected IUserService userService;

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

}
