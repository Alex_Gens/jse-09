package ru.kazakov.iteco.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Status {

    PLANNED("Planned"),
    IN_PROGRESS("In progress"),
    READY("Ready");

    @Getter
    @NotNull
    private String displayName;

}
